# from django.db import models
from shopping.models import Orders,Products,OrdersItems
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = [  'id',
                    'title' ,
                    'description' ,
                    'price' ,
                    'createdAt' ,
                    'updatedAt',
                    'image']



class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = [
            'id',
            'userId',
            'total',
            'products',
            'createdAt',
            'updatedAt',
            'status',
            'modeOfPayment',

        ]
        depth =1
    # def update(self, instance, validated_data):
    #     order=validated_data.pop('Orders')
    #     orders=Orders.objects.create(**validated_data)
    #     # for order  in
    #     return orders

    def update(self, instance, validated_data):

        demo = Orders.objects.get(pk=instance.id)
        Orders.objects.filter(pk=instance.id) .update(**validated_data)
        return demo

class Order_items_Serializer(serializers.ModelSerializer):
    class Meta:
        model = OrdersItems
        fields = [
            'id',
            'userId',
            'productId',
            'Quantity',
            'price',
            'totalCost'
        ]
        depth = 2


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

