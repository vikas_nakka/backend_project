# Create your models here.
from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Products(models.Model):
    title=models.CharField(max_length=30)
    description = models.TextField(max_length=30)
    image=models.ImageField(upload_to='templates/images')
    price=models.IntegerField()
    createdAt = models.DateField(auto_now_add=True)
    updatedAt = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.title}'





class Orders(models.Model):
    PAYMENT_CHOICES = (
        ("cash", "cash"),
        ("paytm", "paytm"),
        ("phonepay", "phonepay"),

    )
    STATUS_CHOICES=(
        ('new','new'),
        ('paid','paid')
    )

    userId=models.ForeignKey(User,on_delete=models.CASCADE)
    total =models.IntegerField()
    products = models.ManyToManyField(Products)
    createdAt = models.DateField(auto_now_add=True)
    updatedAt = models.DateField(auto_now=True)
    status= models.CharField(max_length=10,choices=STATUS_CHOICES)
    modeOfPayment=models.CharField(max_length=10,choices=PAYMENT_CHOICES)
    # Quantity = models.IntegerField(default=0)
    # def total_items(self):
    #   orderitems = self.orderitems.all()
    # total = sum ([item.quantity for items in orderitems])
    # return total


    def __str__(self):
        return f"{self.id}"

class OrdersItems(models.Model):
    userId=models.ForeignKey(User,on_delete=models.CASCADE,default=None)

    # order_id=models.ForeignKey(Orders,on_delete=models.CASCADE)
    productId=models.ForeignKey(Products,on_delete=models.CASCADE)
    Quantity=models.IntegerField()
    # price=models.IntegerField()
    # price = Products.objects.filter(product_id__price=Products.price)
    def price(self):
        return self.productId.price

    def totalCost(self):
        print('productId price:',self.productId.price)
        cost=self.Quantity * self.productId.price
        print('cost:',cost)
        return cost

    def __str__(self):
        return f"{self.productId}"
    # def total(self):
    #     total=self.Quantity * self.price