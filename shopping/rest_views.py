from django.contrib.auth.models import User
from django.http import Http404
from rest_framework.authentication import  TokenAuthentication
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.filters import SearchFilter,OrderingFilter

from shopping.models import Orders,Products,OrdersItems
from shopping.serializers import UserSerializer,ProductSerializer,OrdersSerializer,Order_items_Serializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ProductViewset(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    filter_backends=(SearchFilter,OrderingFilter)
    search_fields=('id','title')


class OrdersViewset(viewsets.ModelViewSet):
    queryset = Orders.objects.all()
    serializer_class = OrdersSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    

    def list(self, request):
     
        object_order =Orders.objects.filter(userId = request.user)
        serializer = OrdersSerializer(object_order, many=True)
        return Response(serializer.data)

    def create(self, request, *args,**kwargs):
        data = request.data
        print('user:',request.user)
        new_order = Orders.objects.create(userId=request.user,total=data['total'],status=data['status'],modeOfPayment=data['modeOfPayment'])
        new_order.save()
        object_order = Orders.objects.filter(userId=request.user)
        print('object_order:',object_order)
        for product in data['products']:
            object_product =Products.objects.get(title=product['title'])
            new_order.products.add(object_product)
        serializer = OrdersSerializer(new_order)

        return Response(serializer.data)

    def retrive(self, request, pk=None):
        queryset = Orders.objects.filter(pk=pk,userId=request.user)
        if not queryset:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = OrdersSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        pass


    def partial_update(self, request, pk=None):
        orders= Orders.objects.get(userId=request.user,pk=pk)
        data = request.data

        try:
            for product in data['products']:
                object_product = Products.objects.get(title=product['title'])
              
                orders.products = object_product
        except Exception:
            print('error',Exception)

        orders.total =data.get('total',orders.total)
        orders.status = data.get('status', orders.status)
        orders.modeOfPayment = data.get('modeOfPayment', orders.modeOfPayment)
        orders.save()

        serialized = OrdersSerializer(request.user, data=request.data, partial=True)
        return Response(serialized.data,status=status.HTTP_202_ACCEPTED)


class Order_item_Viewset(viewsets.ModelViewSet):
    queryset = OrdersItems.objects.all()

    serializer_class = Order_items_Serializer
    authentication_classes =  [TokenAuthentication]
    permission_classes = [IsAuthenticated]


    def create(self, request, *args, **kwargs):
        data = request.data
        new_order_item = OrdersItems.objects.get(userId=request.user,productId = request.Products)
        pass