# Register your models here.
from django.contrib import admin
from shopping.models import Orders,Products,OrdersItems
# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'description', 'image','price','createdAt','updatedAt')

class OrdersAdmin(admin.ModelAdmin):
    list_display = ('id','userId', 'total','status','createdAt','updatedAt','modeOfPayment')

class Orders_items_Admin(admin.ModelAdmin):
    list_display = ('id','userId', 'productId', 'Quantity','price','totalCost')


admin.site.register(Products,ProductAdmin)
admin.site.register(Orders,OrdersAdmin)
admin.site.register(OrdersItems,Orders_items_Admin)